//#include <functional>
//#include <iostream>
//#include <sstream>
//#include <vector>
#include <map>
//Uli
#include <fstream>

#include <chrono>  // for high_resolution_clock
//#include <ompl/base/OptimizationObjective.h>
// Uli astar
#include <queue>
//#include <limits>
//#include <cmath>



#include "v_repPlusPlus/Plugin.h"
#include "plugin.h"
#include "stubs.h"


const float INF = std::numeric_limits<float>::infinity();

const int visionfloatparam_near_clipping = 1000;
const int visionfloatparam_far_clipping = 1001;
// a-star implementation from https://github.com/hjweide/a-star/blob/master/astar.cpp
// represents a single pixel
class Node {
  public:
    int idx;     // index in the flattened grid
    float cost;  // cost of traversing this pixel

    Node(int i, float c) : idx(i),cost(c) {}
};

// the top of the priority queue is the greatest element by default,
// but we want the smallest, so flip the sign
bool operator<(const Node &n1, const Node &n2) {
  return n1.cost > n2.cost;
}

bool operator==(const Node &n1, const Node &n2) {
  return n1.idx == n2.idx;
}

// See for various grid heuristics:
// http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html#S7
// L_\inf norm (diagonal distance)
float linf_norm(float i0, float j0, float i1, float j1) {
  return std::max(std::abs(i0 - i1), std::abs(j0 - j1));
}

// L_1 norm (manhattan distance)
float l1_norm(float i0, float j0, float i1, float j1) {
  return std::abs(i0 - i1) + std::abs(j0 - j1);
}

float euclidean(float x0, float y0, float z0, float x1, float y1, float z1) {
  return sqrt((x0 - x1)*(x0 - x1)+(y0 - y1)*(y0 - y1)+(z0 - z1)*(z0 - z1));
}

// function that takes a function (different heuristics as parameter
float heuristic(float (*f)(float, float, float, float, float, float), float x0, float y0, float z0, float x1, float y1, float z1) {
	return (*f)(x0, y0, z0, x1, y1, z1);
}

inline int cood_1d(int x, int y, int z, int dim_x, int dim_y) // dim_z not needed, is implicit
{
	return z*dim_x*dim_y + y*dim_x + x;
}


// weights:              flattened grid of costs (e.g. high cost for collision, occlusion, close to nearby objects etc)
// dim_x, dim_y, dim_z:  dimensions of grid
// dx, dy, dz:
// start, goal:          index of start/goal in flattened grid if "goal" index is <0, then star will explore ALL (reachable) nodes from start
// paths (output): for each node, stores previous node in path
//extern "C" bool astar(
bool astar(
      const float* weights, const int dim_x, const int dim_y, const int dim_z, //w is dim_x, h is dim_y
	  const float dx, const float dy, const float dz, // distance between grid points for each axis
      const int start, const int goal,
	  const float* distances, const float distPenaltyFactor, const float distNoPenalty,
      int* paths, float* costs_out) {
	int count_main_loop = 0;
  //std::cout << "weights ";
  /*
  for (int i = 0; i< dim_x*dim_y*dim_z; i++)
  {
	  //std::cout << 	  weights[i] << ' ';
	  if (weights[i] < 0)
	  {
		std::cout << "negative weights not allowed" << std::endl;
	    return 0;
	  }

  }
  */
  //std::cout << std::endl;

  //std::cout << "paths ";
  //for (int i = 0; i< dim_x*dim_y*dim_z; i++)
  //{
  //	  std::cout << 	  paths[i] << ' ';
  //}
  //std::cout << std::endl;

  /*
  if(start < 0 or start >= dim_x*dim_y*dim_z)
  {
	  std::cout << " start index " << start << " out of range" << std::endl;
	  return 0;
  }
  if(goal < 0 or goal >= dim_x*dim_y*dim_z)
  {
	  std::cout << " goal index " << goal << " out of range" << std::endl;
	  return 0;
  }
  */

  //const float INF = std::numeric_limits<float>::infinity();

  Node start_node(start, 0.);
  Node goal_node(goal, 0.);

  float* costs = new float[dim_z * dim_y * dim_x];
  bool* explored = new bool[dim_z * dim_y * dim_x];
  for (int i = 0; i < dim_z * dim_y * dim_x; ++i) // for speedup should initialize these outside of astar once and then pass a pointer
  {
    costs[i] = INF;
    explored[i] = false;
  }
  costs[start] = weights[start];//0.;

  float* move1_cost = new float[27]; // costs of moving diagonal is different than moving horizontal
  	  	  	  	  	  	  	  	   // use euclidean distance for all (diagonal) moves
  for (int z = -1; z <= 1; ++z)
  {
	  for (int y = -1; y <= 1; ++y)
	  {
		  for (int x = -1; x <= 1; ++x) // for speedup should initialize these outside of astar once and then pass a pointer
		  {
			  //std::cout << "This should print 0..26: " << std::endl;
			  //std::cout << cood_1d(x+1,y+1,z+1,3,3) << std::endl;
			  //std::cout << "x,y,z" << x << ' '<< y << ' ' << z << std::endl;
			  //std::cout << "dist 1 move: " << euclidean(x*dx,0,y*dy,0,z*dz,0) << std::endl;
			  /*
			  if (cood_1d(x+1,y+1,z+1,3,3) <0 or cood_1d(x+1,y+1,z+1,3,3) > 26)
			  {
				  std::cout << "index out of range" <<std::endl;
				  return false;
			  }
			  */
			  move1_cost[cood_1d(x+1,y+1,z+1,3,3)] = euclidean(x*dx,0,y*dy,0,z*dz,0);
		  }
	  }
  }


  std::priority_queue<Node> nodes_to_visit;
  nodes_to_visit.push(start_node); // fringe

  // Used to calculate valid neighbors (their index in 1D matrix)
  int* nbrs = new int[27]; // (3^3) including "self" for simplicity of calculations (exclude self by setting to -1)
  int nbrs_i = 0;
  bool solution_found = false;

  // temp coordinates in grid to calculate heuristic value
  float x0,y0,z0,x1,y1,z1;

  //int count = 0; // dummy cvounter to break inf loop for debugging\
  variable declaration outside of the loop
  float heuristic_cost;
  int z; // 3d index of 1d index
  int y;
  int x;
  float new_cost;
  float cost_dist_penalty;
  float cost_penalty_cur;   // the cost penalty of the current node: max(0,distNoPenalty-actualDistance[cur])
  float cost_penalty_nbr;   // the cost penalty of the neighbor node: max(0,distNoPenalty-actualDistance[ngbr])
  while (!nodes_to_visit.empty()) {
	  count_main_loop++;
	/*
	if (count++ > 10000000)
	{
		std::cout << "count > 10000000, breaking endless loop ?";
		return 0;
	}
	*/
    // .top() doesn't actually remove the node
    Node cur = nodes_to_visit.top();



    if (cur == goal_node and goal >=0) { // goal=-1 if no goal is specified (case "findAllPaths")
      solution_found = true;
      break;
    }

    nodes_to_visit.pop();

    if(explored[cur.idx])
    {
    	//std::cout << "index already explored " << cur.idx << std::endl;
    	continue;
    }
    else
    {
    	//std::cout << "Now exploring " << cur.idx << std::endl;
    	explored[cur.idx] = true;
    }

    // calculate 3d indices of 1D index // maybe pre-calculate this once and pass to astar in a lookup table?
    z = cur.idx / (dim_y*dim_x);
    y = (cur.idx-z*dim_y*dim_x) / dim_x; // modulu
    x = cur.idx % dim_x; // remainder
    /*
    std::cout << "cur.idx " << cur.idx << std::endl;
    std::cout << "z " << z << std::endl;
    std::cout << "y " << y << std::endl;
    std::cout << "x " << x << std::endl;
    std::cout << "cood_1d (recalculated) " << cood_1d(x,y,z,dim_x,dim_y) << std::endl;
    */
    /*
    for (int i = 0; i <27; ++i)
    {
    	nbrs[i]=-2;
    }
    */
    // check bounds and find up to 26 neighbors: // this could be quite expensive to calculate here inside the main loop
    //bool next_neigbor = false;
    nbrs_i = 0;
    for (int z_d = -1; z_d <= 1; ++z_d)
    {
  	  for (int y_d = -1; y_d <= 1; ++y_d)
  	  {
  		  for (int x_d = -1; x_d <= 1; ++x_d)
  		  {
  			  //next_neigbor = false;
  			  /*
			  {
				  std::cout << "index out of range 2" <<std::endl;
				  return false;
			  }
			  */
  			  //std::cout << "nbrs_i " << nbrs_i << std::endl;
  			  //std::cout << "z_d, y_d, x_d" << z_d << ' ' << y_d << ' ' << x_d << std::endl;
  			  if(z_d==0 and y_d == 0 and x_d == 0) // this is not a neighbor
  			  {
  				nbrs[nbrs_i] = -1;
  			    nbrs_i++; // increment for next index
  			    continue;
  			  }

  			  switch (z_d)
  			  {
  			  	  case -1:
  			  		  if(z <= 0) // checking lower bound
  			  		  {
  			  			nbrs[nbrs_i] = -1;
  			  		    //std::cout << "debug z -1" << std::endl;
  			  		    nbrs_i++;
  			  		    continue;
  			  		  }
  			  		  break;
  			  	  case 1:
  			  		  if(z+1 >= dim_z) // checking upper bound
  			  		  {
  			  			nbrs[nbrs_i] = -1;
  			  		    //std::cout << "debug z +1" << std::endl;
  			  		    nbrs_i++;
			  		    continue;
			  		  }
  			  		  break;
  			  }
  			  //std::cout << "debug before y_d" << std::endl;
  			  switch (y_d)
  			  {
  			  	  case -1:
  			  		  if(y <= 0) // checking lower bound
  			  		  {
  			  	        nbrs[nbrs_i] = -1;
  			  		    //std::cout << "debug y -1" << std::endl;
  			  		    nbrs_i++;
  			  		    continue;
  			  		  }
  			  		  break;
  			  	  case 1:
  			  		  if(y+1 >= dim_y) // checking upper bound
  			  		  {
  			  			nbrs[nbrs_i] = -1;
  	  			  		//std::cout << "debug y +1" << std::endl;
  	  			  		nbrs_i++;
  	  			  		continue;
  			  		  }
  			  		  break;
  			  }
    		  //std::cout << "debug before x_d" << std::endl;
  			  switch (x_d)
  			  {
  			  	  case -1:
  			  		  if(x <= 0) // checking lower bound
  			  		  {
  			  			nbrs[nbrs_i] = -1;
  			  		    //std::cout << "debug x -1" << std::endl;
  			  		    nbrs_i++;
  			  		    continue;
  			  		  }
  			  		  break;
  			  	  case 1:
  			  		  if(x+1 >= dim_x) // checking upper bound
  			  		  {
  			  		    nbrs[nbrs_i] = -1;
  			  		    //std::cout << "debug x +1" << std::endl;
  			  		    nbrs_i++;
  			  		    continue;
  			  		  }
  			  		  break;
  			  }

  			  // All bounds are good, calculate the index in flattened array for neighbor
  			  //std::cout << "nbrs_i " << nbrs_i << "cur.idx + z_d*dim_y*dim_x + y_d*dim_x + x_d " << cur.idx + z_d*dim_y*dim_x + y_d*dim_x + x_d << std::endl;
  			  nbrs[nbrs_i] =cur.idx + z_d*dim_y*dim_x + y_d*dim_x + x_d;
  			  nbrs_i++; // increment for next index

  		  }
  	  }
    }


    /*
    for (int i = 0; i <27; ++i)
    {
    	//std::cout << "nbrs[i]" << nbrs[i] << std::endl;
    	if(nbrs[i] < -1 or nbrs[i] >dim_z*dim_y*dim_x-1)
    	{
    		std::cout << nbrs[i] << " ";
    		std::cout << "nbrs[i] out of range" << std::endl;
    		return 0;
    	}
    }
    */

    // the more hardcoded but tedious approach (but might be faster)
    // offsets in
    //  z, y, x:
    //  z=-1 ###################################
    // -1,-1,-1
    //nbrs[0] = (z > 0 && y > 0 && x > 0)       ? cur.idx - dim_y*dim_x - dim_x - 1  : -1;
    // -1,-1, 0
    // -1,-1,+1
    // -1, 0,-1
    // -1, 0, 0
    // -1, 0,+1
    // -1,+1,-1
    // -1,+1, 0
    // -1,+1,+1

    // z=0 ######################################
    //  0,-1,-1
    //  0,-1, 0
    //  0,-1,+1
    //  0, 0,-1
    //  0, 0, 0
    //nbrs[14] = -1; // this is the node itself, not a neighbor
    //  0, 0,+1
    //  0,+1,-1
    //  0,+1, 0
    //  0,+1,+1

    // z=1  ######################################
    // +1,-1,-1
    // +1,-1, 0
    // +1,-1,+1
    // +1, 0,-1
    // +1, 0, 0
    // +1, 0,+1
    // +1,+1,-1
    // +1,+1, 0
    // +1,+1,+1
    //nbrs[26] = (z + 1 < dim_z && y + 1 < dim_y && x + 1 < dim_x ) ? cur.idx + dim_y*dim_x + dim_x + 1   : -1;

    // the cost penalty of the current node: max(0,distNoPenalty-actualDistance[cur])
    cost_penalty_cur = distNoPenalty-distances[cur.idx];
    if(cost_penalty_cur < 0)
    {
    	cost_penalty_cur = 0;
    }
    for (int i = 0; i < 27; ++i) {
      if (nbrs[i] >= 0) {
    	/*
		if (nbrs[i] > dim_x*dim_y*dim_z-1)
		{
		  std::cout << "nbrs[i] index out of range 3" <<std::endl;
		  return false;
		}
		*/

    	// the cost penalty of the neighbor node: max(0,distNoPenalty-actualDistance[neighbor])
    	cost_penalty_nbr = distNoPenalty-distances[nbrs[i]];
        if(cost_penalty_nbr < 0.0)
        {
        	cost_penalty_nbr = 0.0;
        }
    	// Average penalty of two adjacent nodes times movement distance move1_cost
    	cost_dist_penalty = move1_cost[i]*(cost_penalty_cur+cost_penalty_nbr)/2;

    	if(cost_dist_penalty < 0.0)
    	{
    		std::cout << "Error: cost_dist_penalty < 0" << std::endl;
    		return false;
    	}

        // the sum of the cost so far and the cost of this move (
        new_cost = costs[cur.idx] + weights[nbrs[i]] + move1_cost[i] + distPenaltyFactor*cost_dist_penalty;
        if (new_cost < costs[nbrs[i]]) {
          // estimate the cost to the goal based on legal moves

          //if (diag_ok) {
          if(goal >=0) // if a goal was specified use heuristic (not exploring all nodes)
          {
            z0 = nbrs[i] / (dim_y*dim_x);
            y0 = (nbrs[i]-z0*dim_y*dim_x) / dim_x; // modulu
            x0 = nbrs[i] % dim_x; // remainder

            z1 = goal / (dim_y*dim_x);
            y1 = (goal-z1*dim_y*dim_x) / dim_x; // modulu
            x1 = goal % dim_x; // remainder
            //euclidean arguments: x0,y0,z0 x1,y1,z1
            heuristic_cost =  euclidean(x0*dx,y0*dy,z0*dz,
                                      x1*dx,y1*dy,z1*dz);

            // paths with lower expected cost are explored first
            //float priority = new_cost + heuristic_cost;
            nodes_to_visit.push(Node(nbrs[i], new_cost + heuristic_cost));
          }
	      else
	      {
	        nodes_to_visit.push(Node(nbrs[i], new_cost)); // no heuristic if exploring all nodes (no goal specified)
	      }

          costs[nbrs[i]] = new_cost;
          //std::cout << "nbrs[i] " << nbrs[i] << std::endl;
          //std::cout << "cur.idx " << cur.idx << std::endl;
          //std::cout << "costs[nbrs[i]] " << costs[nbrs[i]] << std::endl;

          paths[nbrs[i]] = cur.idx;
        }
      }
    }
  }
  int count_explored = 0;
  for(int i=0; i < dim_x*dim_y*dim_z; i++)
  {
	  // Copy cost results to output variable

	  //std::cout << ",paths[" << i << "] = " << " " << paths[i];
	  //std::cout << ",costs[" << i << "] = " << " " << costs[i];
	  //std::cout << std::endl;
	  costs_out[i]=costs[i];
	  if(explored[i])
	    count_explored++;

  }
  //std::cout << "count_main_loop " << count_main_loop << std::endl;
  //std::cout << "count_explored " << count_explored << std::endl;
  delete[] explored;
  delete[] move1_cost;
  delete[] costs;
  delete[] nbrs;

  return solution_found;
}


long int global_counter = 0; // For debugging, e.g. o count things like how many times collision checking done
long int global_count_collision = 0;

struct LuaCallbackFunction
{
    // name of the Lua function
    std::string function;
    // id of the V-REP script where the function is defined in
    simInt scriptId;
};

struct ObjectDefHeader
{
    // internal handle of this object (used by the plugin):
    simInt handle;
    // name of this object:
    std::string name;
    // objects created during simulation will be destroyed when simulation terminates:
    bool destroyAfterSimulationStop;
};



class stateWithInfo
{
	// Class that stores info about states
	// Info is
	// x,y,z,t: for position and orientation
	// col: if 1then state is in collision
	// vis_target: Number of visible pixels of target
	// vis_obj: Number of visible pixels of object in gripper
	//			(Count only the pixels of visibility object near bottom of object)
	// dist: The distance between Gripper/obj to environment (clutter/target)
	//		 (Not implemented yet)

	public:
	simFloat x;
	simFloat y;
	simFloat z;
	simFloat t;
	simInt col;
	simInt vis_target;
	simInt vis_obj;
	simFloat dist;
	stateWithInfo() // Default Constructor
	{
		x=0;
		y=0;
		z=0;
		t=0;
		col=-1;
		vis_target=-1;
		vis_obj=-1;
		dist=INF;
	}

	void setXYZT(simFloat x1,simFloat y1,simFloat z1,simFloat t1)
	{
		x=x1; y=y1; z=z1; t=t1;
	}

	void setCol(simInt col1)
	{
		col=col1;
	}

	void setVis_target(simInt vis1)
	{
		vis_target=vis1;
	}

	void setVis_obj(simInt vis1)
	{
		vis_obj=vis1;
	}

	void setDist(simFloat dist1)
	{
		dist=dist1;
	}

};


class distCheckConfig
{
	// Class that stores info about config related to distance calculation
	// and cost parameters for states close to obstacles (small distances)

	public:
    // The threshold used for checkDistance calls.
    // If the actual distance is greater than maxDistance, then the actual distance value is not calculated.
    // If maxDistance is set to 0 or negative, then no threshold is used (distance values are always calculated)
    simFloat maxDistance;

    // Closeness to collision penalty variables
    // If distance of gridpoint to clutter is < distNoPenalty,
    // then a penalty is added to the movement cost.
    // The penalty is:  distPenaltyFactor*(distNoPenalty-actualDistance)
    // 					for distNoPenalty-actualDistance > 0
    // No penalty for distNoPenalty-actualDistance <= 0
    // Cost to move to gridpoint with actualDistance to clutter:
    // normalMoveCost + penalty =
    // normalMoveCost + distPenaltyFactor*(distNoPenalty-actualDistance)*normalMoveCost =
    // normalMoveCost * (1 + distPenaltyFactor*(distNoPenalty-actualDistance) )
    simFloat distNoPenalty; // The threshold in meters, where a penalty will be applied
    simFloat distPenaltyFactor; // The max penalty factor for distance 0 (decreases linearly until distance distNoPenalty)



	distCheckConfig() // Default Constructor
	{
		maxDistance = 0.0; // 0.0 means "noDistance threshold: all distances are calculated.
		distPenaltyFactor=0.0;
		distNoPenalty=0.0;
	}
};

struct TaskDef
{
    ObjectDefHeader header;
    bool setup_called = false;
    simInt robotHandle;
    simInt refFrameHandle;
    std::vector<simInt> collisionPairHandles;

    simInt depthCameraHandle; // camera for regular depth image generation
    simInt depthCameraHandleVisibility; // camera that sees the "visibility cube" of target for checking if occluded by clutter etc
    simInt depthCameraHandleVisibility_man_obj; // camera that sees the "visibility cube" of manipulation object in
    											// gripper for checking if occluded by clutter etc

    simFloat resolution_x; // resolution of camera
    simFloat resolution_y;
    simFloat z_near;	// near clipping distance
    simFloat z_far; // far clipping distance

    // dimensions of col_grid
    simInt dim_x;
    simInt dim_y;
    simInt dim_z;
    simInt dim_t; // used for theta
    simInt dim_total; // product of the above dimensions


    // Should collision, visibility or distances be checked?
    simInt checkCol;
    simInt checkVis;
    simInt checkDist;

    // Should collision cost be considered by the planner?
    simInt use_collision_cost_for_planner;

    // class that stores distance check related parameters
    distCheckConfig distConfig;

    // grid to store collisions etc
    //std::vector<simInt> col_grid;
    std::vector<float> cost_grid; // sum over cost for collision/visibility etc. This grid is used to search lowest cost path
    std::vector<float> cost_grid_dist; // sum over cost for collision/visibility etc. This grid is used to search lowest cost path

    std::vector<stateWithInfo> statesGridWithInfo;



    // lower bounds of search space:
    std::vector<simFloat> boundsLow;
    // upper bounds of search space:
    std::vector<simFloat> boundsHigh;



    // vectors to store values of x,y,z,theta corresponding to the index of col_grid
	std::vector<simFloat> values_x;
	std::vector<simFloat> values_y;
	std::vector<simFloat> values_z;
	std::vector<simFloat> values_t;

    // the distances (deltas) between grid points in each axis
    simFloat dx;
    simFloat dy;
    simFloat dz;
    simFloat dt;

    // used by "find"allPaths", after grid (collisions) has been build
    std::vector<int> pathsAll;
    std::vector<float> pathsAllCosts; // specifies the costs to the goal (where the cost at start is just the value of cost_grid[start], e.g. if in collision).



	// for "findPath", after grid has been build
	std::vector<simFloat> stateStart;
	std::vector<simFloat> stateGoal;

	// for a solution (vector of states from start to goal)
    std::vector<simFloat> pathStates;


};


std::map<simInt, TaskDef *> tasks;

simInt nextTaskHandleUli = 14000;

// this function will be called at simulation end to destroy objects that
// were created during simulation, which otherwise would leak indefinitely:
template<typename T>
void destroyTransientObjects(std::map<simInt, T *>& c)
{
    std::vector<simInt> transientObjects;

    for(typename std::map<simInt, T *>::const_iterator it = c.begin(); it != c.end(); ++it)
    {
        if(it->second->header.destroyAfterSimulationStop)
            transientObjects.push_back(it->first);
    }

    for(size_t i = 0; i < transientObjects.size(); i++)
    {
        simInt key = transientObjects[i];
        T *t = c[key];
        c.erase(key);
        delete t;
    }
}

void destroyTransientObjects()
{
    destroyTransientObjects(tasks);
}



void createTask(SScriptCallBack *p, const char *cmd, createTask_in *in, createTask_out *out)
{
	TaskDef *taskUli = new TaskDef();
	taskUli->header.destroyAfterSimulationStop = simGetSimulationState() != sim_simulation_stopped;
	taskUli->header.handle = nextTaskHandleUli++;
	taskUli->header.name = in->name;
    tasks[taskUli->header.handle] = taskUli;
    out->taskHandle = taskUli->header.handle;
}



TaskDef * getTask(simInt taskHandle)
{
    if(tasks.find(taskHandle) == tasks.end())
        throw std::string("Invalid task handle.");

    return tasks[taskHandle];
}



void destroyTask(SScriptCallBack *p, const char *cmd, destroyTask_in *in, destroyTask_out *out)
{
    TaskDef *task = getTask(in->taskHandle);

    tasks.erase(in->taskHandle);
    delete task;
}


//##################################################
// Uli's code here to do my own "path planner"
//####################################################

bool checkCollision(TaskDef *task)
{
    // check collisions:
    bool inCollision = false;
    for(size_t i = 0; i < task->collisionPairHandles.size() / 2; i++)
    {
        if(task->collisionPairHandles[2 * i + 0] >= 0)
        {
        	//std::cout << "Measuring execution time " << std::endl;
        	//std::chrono::_V2::system_clock::time_point start = std::chrono::high_resolution_clock::now();
            int r = simCheckCollision(task->collisionPairHandles[2 * i + 0], task->collisionPairHandles[2 * i + 1]);
            //std::chrono::_V2::system_clock::time_point end = std::chrono::high_resolution_clock::now();
            //std::cout << "Collision (simCheckCollision) check took "
            //          << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
    		//		  << "us" << std::endl;
            if(r > 0)
            {
                inCollision = true;
                break;
            }
        }
        if(inCollision) break;
    }

    return inCollision;
}



// function to setup/stores the parameters
void setupValidStatesOnGrid(SScriptCallBack *p, const char *cmd, setupValidStatesOnGrid_in *in, setupValidStatesOnGrid_out *out)
{
	TaskDef *task = getTask(in->taskHandle);
	task->setup_called=true;
	task->robotHandle = in->robotHandle;
	task->refFrameHandle = in->refFrameHandle;
	task->collisionPairHandles = in->collisionPairHandles;
	task->depthCameraHandle = in->depthCameraHandle;
	task->depthCameraHandleVisibility = in->depthCameraHandleVisibility;
	task->depthCameraHandleVisibility_man_obj = in->depthCameraHandleVisibility_man_obj;

	// Getting resolutions of sensors (and check if they are the same)
	int resolution[2];
	int resolution2[2];
	int resolution3[2];
	simGetVisionSensorResolution(task->depthCameraHandle,resolution);
	simGetVisionSensorResolution(task->depthCameraHandleVisibility,resolution2);
	simGetVisionSensorResolution(task->depthCameraHandleVisibility_man_obj,resolution3);
	if(resolution[0] != resolution2[0] or resolution[1] != resolution2[1] or
	   resolution[0] != resolution3[0] or resolution[1] != resolution3[1])
	{
		std::cout << "Error in checkVisibilityInternal: Resolution of both sensors is not equal" << std::endl;
	}
	else
	{
	    task->resolution_x = resolution[0];
	    task->resolution_y = resolution[1];
		std::cout << "Sensor resolution : " << task->resolution_x << " " << task->resolution_y <<std::endl;
	}

	// Getting clipping planes of sensors (and check if they are the same)
	float z_near1 = 0.0;
	float z_far1 = 0.0;
	float z_near2 = 0.0;
	float z_far2 = 0.0;
	float z_near3 = 0.0;
	float z_far3 = 0.0;
	simGetObjectFloatParameter(task->depthCameraHandle,visionfloatparam_near_clipping,&z_near1);
	simGetObjectFloatParameter(task->depthCameraHandle,visionfloatparam_far_clipping,&z_far1);

	simGetObjectFloatParameter(task->depthCameraHandleVisibility,visionfloatparam_near_clipping,&z_near2);
	simGetObjectFloatParameter(task->depthCameraHandleVisibility,visionfloatparam_far_clipping,&z_far2);
	simGetObjectFloatParameter(task->depthCameraHandleVisibility_man_obj,visionfloatparam_near_clipping,&z_near3);
	simGetObjectFloatParameter(task->depthCameraHandleVisibility_man_obj,visionfloatparam_far_clipping,&z_far3);
	if(z_near1 != z_near2 or z_far1  != z_far2 or
	   z_near1 != z_near3 or z_far1  != z_far3)
	{
		std::cout << "Warning in checkVisibilityInternal: Clipping planes of both sensors is not equal" << std::endl;
	}
	else
	{
		task->z_near = z_near1;
		task->z_far = z_far1;
		std::cout << "Clipping planes : " << task->z_near << " " << task->z_far <<std::endl;
	}


	task->dim_x = in->dim_x;
	task->dim_y = in->dim_y;
	task->dim_z = in->dim_z;
	task->dim_t = in->dim_t; //dim_t not used yet
	task->dim_total = task->dim_x*task->dim_y*task->dim_z*task->dim_t;
	std::cout << "Reserving statesGridWithInfo vector with "
			  << task->dim_total
			  << " entries" << std::endl;
	task->checkCol = in->checkCol;
	task->checkVis = in->checkVis;
	task->checkDist = in->checkDist;

	task->use_collision_cost_for_planner = in->use_collision_cost_for_planner;

	task->distConfig.maxDistance = in->maxDistance;
	task->distConfig.distNoPenalty = in->distNoPenalty;
	task->distConfig.distPenaltyFactor = in->distPenaltyFactor;

	//task->col_grid.reserve(task->dim_total);
	task->statesGridWithInfo.reserve(task->dim_total);


	for(int i=0; i<task->dim_total; i++)
	{
	   stateWithInfo s;

       task->statesGridWithInfo.push_back(s);
	}

	if (task->statesGridWithInfo.size() != task->dim_total)
	{
		throw std::string("Size of statesGridWithInfo not as expected");
	}


	task->boundsLow = in->boundsLow;
	task->boundsHigh = in->boundsHigh;

	// setting deltas (distances between grid points in each axis)
	task->dx = (task->boundsHigh[0]-task->boundsLow[0]) / (task->dim_x-1);
	std::cout << "task->dx " << task->dx << std::endl;
	task->dy = (task->boundsHigh[1]-task->boundsLow[1]) / (task->dim_y-1);
	std::cout << "task->dy " << task->dy << std::endl;
	task->dz = (task->boundsHigh[2]-task->boundsLow[2]) / (task->dim_z-1);
	std::cout << "task->dz " << task->dz << std::endl;

	// Setting index to values vectors
	for(int i=0; i<task->dim_x; i++)
	{
		//task->values_x.push_back(task->boundsLow[0]+float(i)/(task->dim_x-1)*(task->boundsHigh[0]-task->boundsLow[0]));
		task->values_x.push_back(task->boundsLow[0]+i*task->dx);
	}
	for(int i=0; i<task->dim_y; i++)
	{
		task->values_y.push_back(task->boundsLow[1]+i*task->dy);
	}
	for(int i=0; i<task->dim_z; i++)
	{
		task->values_z.push_back(task->boundsLow[2]+i*task->dz);
	}

	task->pathsAll.reserve(task->dim_total);
	task->pathsAllCosts.reserve(task->dim_total);




	// Debugging/checking values:
	/*
	for(int i=0; i<task->dim_x; i++)
	{
		std::cout << "values_x[" << i << "],"  << task->values_x.at(i) << std::endl;
	}

	for(int i=0; i<task->dim_y; i++)
	{
		std::cout << "values_y[" << i << "],"  << task->values_y.at(i) << std::endl;
	}

	for(int i=0; i<task->dim_z; i++)
	{
		std::cout << "values_z[" << i << "],"  << task->values_z.at(i) << std::endl;
	}
	*/

}


// This checks how many pixels of a sphere placed at the target is visible (not occluded by clutter or out of camera view)
// This works by generating two depth images
// first image is the original depth image
// second image is a depth image that that can also detect a sphere that is only visible in the second image.
// Calculating the pixelwise differences of the image, the number of pixels that are different indicate how many pixels of the
// checkVisibility returns the number of visible pixels
int checkVisibilityInternalNotOptimized(int depthCameraHandle, int depthCameraHandleVisibility, double pixelDeltaThreshold)
{
	//std::cout << "checkVisibilityInternal" << std::endl;
	float* auxValues1=NULL;
	float* auxValues2=NULL;
	int* auxValuesCount1=NULL;
	int* auxValuesCount2=NULL;
	float* depthBuffer1=NULL;
	float* depthBuffer2=NULL;

	simHandleVisionSensor(depthCameraHandle,&auxValues1,&auxValuesCount1);
	simHandleVisionSensor(depthCameraHandleVisibility,&auxValues2,&auxValuesCount2);
	simReleaseBuffer((char*)auxValues1);
	simReleaseBuffer((char*)auxValues2);
	simReleaseBuffer((char*)auxValuesCount1);
	simReleaseBuffer((char*)auxValuesCount2);

	int resolution1[2];
	int resolution2[2];
	simGetVisionSensorResolution(depthCameraHandle,resolution1);
	simGetVisionSensorResolution(depthCameraHandleVisibility,resolution2);
	//std::cout << "pixelDeltaThreshold " << pixelDeltaThreshold << std::endl;
	//std::cout << "resolution " << resolution1[0] << ' ' << resolution1[1] << std::endl;
	if(resolution1[0] != resolution2[0] or resolution1[1] != resolution2[1])
	{
		std::cout << "Error in checkVisibilityInternalNotOptimized: Resolution of both sensors is not equal" << std::endl;
	}

	float z_near1 = 0.0;
	float z_near2 = 0.0;
	float z_far1 = 0.0;
	float z_far2 = 0.0;

	//const int visionfloatparam_near_clipping = 1000;
	//const int visionfloatparam_far_clipping = 1001;
	simGetObjectFloatParameter(depthCameraHandle,visionfloatparam_near_clipping,&z_near1);
	simGetObjectFloatParameter(depthCameraHandle,visionfloatparam_far_clipping,&z_far1);
	simGetObjectFloatParameter(depthCameraHandleVisibility,visionfloatparam_near_clipping,&z_near2);
	simGetObjectFloatParameter(depthCameraHandleVisibility,visionfloatparam_far_clipping,&z_far2);

	//std::cout << "z_near1,z_far1" << z_near1 << ' ' << z_far1 << std::endl;
	if(z_near1 != z_near2 or z_far1 != z_far2)
	{
		std::cout << "Warning in checkVisibilityInternal: Clipping planes of both sensors is not equal" << std::endl;
	}


	float depth_meters1=0.0;
	float depth_meters2=0.0;
	int NumPixelsVisible = 0;

	depthBuffer1 = simGetVisionSensorDepthBuffer(depthCameraHandle);
	depthBuffer2 = simGetVisionSensorDepthBuffer(depthCameraHandleVisibility);

	if(depthBuffer1 != NULL and depthBuffer2 !=NULL)
	{

		for(int i=0; i<resolution1[0]*resolution1[1]; i++)
		{
			depth_meters1 = depthBuffer1[i] * (z_far1-z_near1) + z_near1;
			depth_meters2 = depthBuffer2[i] * (z_far2-z_near2) + z_near2;

			if(std::abs(depth_meters1-depth_meters2) >= pixelDeltaThreshold)
			{

				//std::cout << "depthBuffer1[" << i << "]" <<  depthBuffer1[i] << "depth_meters " << depth_meters1 << std::endl;
				//std::cout << "depthBuffer2[" << i << "]" <<  depthBuffer2[i] << "depth_meters " << depth_meters2 << std::endl;
				NumPixelsVisible+=1;
			}
			/*
			if(std::abs(depth_meters1-depth_meters2) <= pixelDeltaThreshold and std::abs(depth_meters1-depth_meters2) > 0.0001)
			{
				std::cout << "deltas < " << pixelDeltaThreshold << ": " << std::abs(depth_meters1-depth_meters2) << std::endl;
			}

			if(i < 10) // debug
			{
				std::cout << "depthBuffer1[" << i << "]" <<  depthBuffer1[i] << "depth_meters " << depth_meters1 << std::endl;
				std::cout << "depthBuffer2[" << i << "]" <<  depthBuffer2[i] << "depth_meters " << depth_meters2 << std::endl;
			}
			*/
		}

	}
	else
	{
		std::cout << "depthBuffer1 or depthBuffer2 is NULL" << std::endl;
	}



	simReleaseBuffer((char*)depthBuffer1);
	simReleaseBuffer((char*)depthBuffer2);


	//std::cout << "NumPixelsVisible " << NumPixelsVisible << std::endl;
	return NumPixelsVisible;
}

// plugin function to check for visibility
// Does not require planning/task handle
// This is used to check for visibility during scene generation BEFORE planning in the scene (finding trajectories)
void checkVisibilityNoTask(SScriptCallBack *p, const char *cmd, checkVisibilityNoTask_in *in, checkVisibilityNoTask_out *out)
{
	out->NumPixelsVisible = checkVisibilityInternalNotOptimized(in->depthCameraHandle,in->depthCameraHandleVisibility,in->pixelDeltaThreshold);
}

// This checks how many pixels of a sphere placed at the target is visible (not occluded by clutter or out of camera view)
// This works by generating two depth images
// first image is the original depth image
// second image is a depth image that that can also detect a sphere that is only visible in the second image.
// Calculating the pixelwise differences of the image, the number of pixels that are different indicate how many pixels of the
// checkVisibility returns the number of visible pixels
// similar to checkVisibilityInternal, but gets depth buffer from main camera only once (faster than calling checkVisibilityInternal twice).

// Inputs:
// - task: pointer to the taskDef that contains information like resolution of the camera
// - depthCameraHandle: The handle to the main depth camera (that does not see special visibility objects)
// - depthCameraHandle1: The handle to depth camera that sees a special object (e.g. cube at target object)
// - depthCameraHandle2: The handle to depth camera that sees a special object (e.g. cube at object in gripper)
// - ret_NumPixelsVisible1: Variable to store output for num of pixels visible using depthCameraHandle1
// - ret_NumPixelsVisible2: Variable to store output for num of pixels visible using depthCameraHandle2
//
// Returns:
// - Always true (see ret_NumPixelsVisible1 and ret_NumPixelsVisible2 for the results)

bool checkVisibilityTargetAndObj(TaskDef *task, int depthCameraHandle, int depthCameraHandleVisibility1, int depthCameraHandleVisibility2,
										double pixelDeltaThreshold1, double pixelDeltaThreshold2,
										int& ret_NumPixelsVisible1, int& ret_NumPixelsVisible2)
{
	float* auxValues=NULL;
	float* auxValues1=NULL;
	float* auxValues2=NULL;
	int* auxValuesCount=NULL;
	int* auxValuesCount1=NULL;
	int* auxValuesCount2=NULL;
	float* depthBuffer=NULL;
	float* depthBuffer1=NULL;
	float* depthBuffer2=NULL;

	// This is the most time consuming operation
	simHandleVisionSensor(depthCameraHandle,&auxValues,&auxValuesCount);
	simHandleVisionSensor(depthCameraHandleVisibility1,&auxValues1,&auxValuesCount1);
	simHandleVisionSensor(depthCameraHandleVisibility2,&auxValues2,&auxValuesCount2);

	simReleaseBuffer((char*)auxValues);
	simReleaseBuffer((char*)auxValues1);
	simReleaseBuffer((char*)auxValues2);
	simReleaseBuffer((char*)auxValuesCount);
	simReleaseBuffer((char*)auxValuesCount1);
	simReleaseBuffer((char*)auxValuesCount2);

	float depth_meters=0.0;
	float depth_meters1=0.0;
	float depth_meters2=0.0;
	int NumPixelsVisible1 = 0;
	int NumPixelsVisible2 = 0;

	depthBuffer = simGetVisionSensorDepthBuffer(depthCameraHandle);
	depthBuffer1 = simGetVisionSensorDepthBuffer(depthCameraHandleVisibility1);
	depthBuffer2 = simGetVisionSensorDepthBuffer(depthCameraHandleVisibility2);

	if(depthBuffer1 != NULL and depthBuffer1 != NULL and depthBuffer2 !=NULL)
	{

		for(int i=0; i<task->resolution_x*task->resolution_y; i++)
		{
			depth_meters  = depthBuffer[i]  * (task->z_far-task->z_near) + task->z_near;
			depth_meters1 = depthBuffer1[i] * (task->z_far-task->z_near) + task->z_near;
			depth_meters2 = depthBuffer2[i] * (task->z_far-task->z_near) + task->z_near;

			if(std::abs(depth_meters-depth_meters1) >= pixelDeltaThreshold1)
			{
				NumPixelsVisible1 += 1;
			}
			if(std::abs(depth_meters-depth_meters2) >= pixelDeltaThreshold2)
			{
				NumPixelsVisible2 += 1;
			}
		}

	}
	else
	{
		std::cout << "depthBuffer or depthBuffer1 or depthBuffer2 is NULL" << std::endl;
	}
	ret_NumPixelsVisible1 = NumPixelsVisible1;
	ret_NumPixelsVisible2 = NumPixelsVisible2;
	simReleaseBuffer((char*)depthBuffer);
	simReleaseBuffer((char*)depthBuffer1);
	simReleaseBuffer((char*)depthBuffer2);


	// Outputs are:
	// ret_NumPixelsVisible1 and ret_NumPixelsVisible2 (passed as references)
	return true;
}

// This checks how many pixels of a sphere placed at the target is visible (not occluded by clutter or out of camera view)
// This works by generating two depth images
// first image is the original depth image
// second image is a depth image that that can also detect a sphere that is only visible in the second image.
// Calculating the pixelwise differences of the image, the number of pixels that are different indicate how many pixels of the
// checkVisibility returns the number of visible pixels
int checkVisibilityInternal(TaskDef *task,int depthCameraHandle, int depthCameraHandleVisibility, double pixelDeltaThreshold)//TaskDef *task)
{
	float* auxValues1=NULL;
	float* auxValues2=NULL;
	int* auxValuesCount1=NULL;
	int* auxValuesCount2=NULL;
	float* depthBuffer1=NULL;
	float* depthBuffer2=NULL;


	// This is the most time consuming operation
	simHandleVisionSensor(depthCameraHandle,&auxValues1,&auxValuesCount1);
	simHandleVisionSensor(depthCameraHandleVisibility,&auxValues2,&auxValuesCount2);

	simReleaseBuffer((char*)auxValues1);
	simReleaseBuffer((char*)auxValues2);
	simReleaseBuffer((char*)auxValuesCount1);
	simReleaseBuffer((char*)auxValuesCount2);

	float depth_meters1=0.0;
	float depth_meters2=0.0;
	int NumPixelsVisible = 0;

	depthBuffer1 = simGetVisionSensorDepthBuffer(depthCameraHandle);
	depthBuffer2 = simGetVisionSensorDepthBuffer(depthCameraHandleVisibility);

	if(depthBuffer1 != NULL and depthBuffer2 !=NULL)
	{

		for(int i=0; i<task->resolution_x*task->resolution_y; i++)
		{
			depth_meters1 = depthBuffer1[i] * (task->z_far-task->z_near) + task->z_near;
			depth_meters2 = depthBuffer2[i] * (task->z_far-task->z_near) + task->z_near;

			if(std::abs(depth_meters1-depth_meters2) >= pixelDeltaThreshold)
			{
				NumPixelsVisible+=1;
			}
		}

	}
	else
	{
		std::cout << "depthBuffer1 or depthBuffer2 is NULL" << std::endl;
	}

	simReleaseBuffer((char*)depthBuffer1);
	simReleaseBuffer((char*)depthBuffer2);


	//std::cout << "NumPixelsVisible " << NumPixelsVisible << std::endl;
	return NumPixelsVisible;
}



// plugin function to check for visibility
// require pplanning/task handle
void checkVisibility(SScriptCallBack *p, const char *cmd, checkVisibility_in *in, checkVisibility_out *out)
{
	TaskDef *task = getTask(in->taskHandle);
	//std::cout << "checkVisibility callback" << std::endl;
	//std::cout << "Measuring execution time " << std::endl;
	//std::chrono::_V2::system_clock::time_point start = std::chrono::high_resolution_clock::now();
	//TaskDef *task = getTask(in->depthCameraHandle);
	out->NumPixelsVisible = checkVisibilityInternal(task,in->depthCameraHandle,in->depthCameraHandleVisibility,in->pixelDeltaThreshold);

    //std::chrono::_V2::system_clock::time_point end = std::chrono::high_resolution_clock::now();
    //std::cout << "checkVisibility check took "
    //          << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
	//		  << "us" << std::endl;
}


simFloat checkDistanceInternal(TaskDef *task, simFloat maxDistance)
{
	simFloat distance = 0.0;
	simFloat distanceData[7];


	int r = simCheckDistance(task->collisionPairHandles[0], task->collisionPairHandles[1], maxDistance, distanceData);

	//std::cout << "maxDistance " << maxDistance << std::endl;
	//std::cout << "r " << r << std::endl;

	if(r == 1)
	{
		//std::cout << "distanceData: ";
		//for(int i = 0; i< 7; i++)
		//	std::cout << distanceData[i] << std::endl;
		distance = distanceData[6];

	}
	else if(r == 0)
	{
		distance = maxDistance;
	}
	else
	{
		std::cout << "simCheckDistance (called by checkDistanceInternal) was not successful";
	}
	//std::cout << "distance " << distance << std::endl;
    return distance;
}


// plugin function to check for distance between gripper/object in gripper and table/clutter
void checkDistance(SScriptCallBack *p, const char *cmd, checkDistance_in *in, checkDistance_out *out)
{
	//std::cout << "checkDistance callback" << std::endl;
	//std::cout << "Measuring execution time " << std::endl;
	//std::chrono::_V2::system_clock::time_point start = std::chrono::high_resolution_clock::now();
	TaskDef *task = getTask(in->taskHandle);
	simFloat maxDistance = 0.0;
	if(in->maxDistance == -1.0) // default parameter, use parameter specified with setupValidStatesOnGrid
	{
		maxDistance = task->distConfig.maxDistance;
	}
	else
	{
		maxDistance = in->maxDistance;
	}
	//std::cout << "maxDistance " << maxDistance << std::endl;
	out->distance = checkDistanceInternal(task,maxDistance);
    //std::chrono::_V2::system_clock::time_point end = std::chrono::high_resolution_clock::now();
    //std::cout << "checkDistance check took "
    //          << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
	//		  << "us" << std::endl;
}

// Function prototype to be used in queryColAndDistance
void findClosestGridPoint(TaskDef *task, std::vector<simFloat> state_in, std::vector<simInt> *grid_coordinate_out);


// like checkDistance, but looks up pre-calculated value for collision and distance of nearest grid-point to a input state
void queryColAndDistance(SScriptCallBack *p, const char *cmd, queryColAndDistance_in *in, queryColAndDistance_out *out)
{
	TaskDef *task = getTask(in->taskHandle);
	// find closest gridpoint (index in 3d grid) for a state

	std::vector<simInt> grid_coordinate_state = {-1,-1,-1};
	findClosestGridPoint(task,in->state,&grid_coordinate_state);
	int i = cood_1d(grid_coordinate_state.at(0),grid_coordinate_state.at(1),grid_coordinate_state.at(2),task->dim_x,task->dim_y);
	out->collision = task->statesGridWithInfo[i].col;
	out->distance = task->statesGridWithInfo[i].dist;
}

// Convert 1d coordinate to 3d:
// inputs:
// idx_1D (1D coordinate in flattened array)
// dim_x (dimension of 3D array in x-axis)
// dim_y (dimension of 3D array in y-axis)
// Note that last dim (dim_z) is not needed, because it is implicit
// x,y,z passed by reference to write the output to
//
// outputs:
// values referenced with x,y,z are updated)

void coordinate_1D_to_3D(int idx_1D,int dim_x,int dim_y, int& x, int& y, int& z)
{
    // calculate 3d indices of 1D index
    z = idx_1D / (dim_y*dim_x);
    y = (idx_1D-z*dim_y*dim_x) / dim_x; // modulu
    x = idx_1D % dim_x; // remainder
}


// Convert 3d coordinate to 1d is simply:
// task->dim_x*task->dim_y*z + task->dim_y*y + x


// Done: function that checks distance between pairs of objects

// Done: function that checks for visibility of object in camera

void sampleValidStatesOnGrid(SScriptCallBack *p, const char *cmd, sampleValidStatesOnGrid_in *in, sampleValidStatesOnGrid_out *out)
{
	TaskDef *task = getTask(in->taskHandle);
	if (!task->setup_called)
	{
        throw std::string("Function setupValidStatesOnGrid must be called before sampleValidStatesOnGrid");
	}

	std::cout << "sampleValidStatesOnGrid callback" << std::endl;
	std::cout << "Measuring execution time " << std::endl;
	std::chrono::_V2::system_clock::time_point start = std::chrono::high_resolution_clock::now();

	simFloat pos_init[3], orient_init[4];
	// Save initial state:
	simGetObjectPosition(task->robotHandle, task->refFrameHandle, &pos_init[0]);
	simGetObjectOrientation(task->robotHandle, task->refFrameHandle, &orient_init[0]); // Euler angles

	simFloat pos[3];

	bool col = 0;
	int NumPixelsVisible1_old = 0;
	int NumPixelsVisible2_old = 0;
	int NumPixelsVisible1 = 0;
	int NumPixelsVisible2 = 0;
	simFloat distance = 0;
	for(int z=0; z<task->dim_z; z++)
	{
		pos[2] = task->values_z[z];
		for(int y=0; y<task->dim_y; y++)
		{
			pos[1] = task->values_y[y];
			for(int x=0; x<task->dim_x; x++)
			{
	        	//std::cout << "Measuring execution time " << std::endl;
	        	//std::chrono::_V2::system_clock::time_point start = std::chrono::high_resolution_clock::now();

				pos[0] = task->values_x[x];
				//Add state with collision info
				stateWithInfo s;
				s.setXYZT(pos[0],pos[1],pos[2],0);

                //std::cout << "Update state to pos: " << pos[2] << ", " << pos[1] << ", " << pos[0] << std::endl; // Debug reading and writing of states
                simSetObjectPosition(task->robotHandle, task->refFrameHandle, &pos[0]);

				// Check for collision
            	if(task->checkCol)
            	{
            		col = checkCollision(task);
            		s.setCol(col);
            	}

        		if(task->checkVis)
        		{

        			// Check for visibility of target (numbers of target pixels visible)
        			//NumPixelsVisible1_old = checkVisibilityInternal(task,task->depthCameraHandle,
        			//											task->depthCameraHandleVisibility,
					//											0.01); // threshold for dist values
        			//std::cout << "NumPixelsVisible1_old: " << NumPixelsVisible1_old << std::endl;
        			//s.setVis_target(NumPixelsVisible1);

        			// Check for visibility of obj in gripper (e.g. numbers of visibility object at bottom of peg pixels visible)
        			//NumPixelsVisible2_old = checkVisibilityInternal(task,task->depthCameraHandle,
        			//											task->depthCameraHandleVisibility_man_obj,
					//											0.001); // threshold for dist values
        			//std::cout << "NumPixelsVisible2_old: " << NumPixelsVisible2_old << std::endl;
        			//s.setVis_obj(NumPixelsVisible2);


        			checkVisibilityTargetAndObj(task, task->depthCameraHandle,
        					task->depthCameraHandleVisibility, task->depthCameraHandleVisibility_man_obj,
							0.01, 0.001,
							NumPixelsVisible1, NumPixelsVisible2);
        			s.setVis_target(NumPixelsVisible1);
        			s.setVis_obj(NumPixelsVisible2);
        			//std::cout << "NumPixelsVisible1: "     << NumPixelsVisible1 << std::endl;
        			//std::cout << "NumPixelsVisible1_old: " << NumPixelsVisible1_old << std::endl;
        			//std::cout << "NumPixelsVisible2: "     << NumPixelsVisible2 << std::endl;
        			//std::cout << "NumPixelsVisible2_old: " << NumPixelsVisible2_old << std::endl << std::endl;
        		}

        		if(task->checkDist)
        		{
        			distance = checkDistanceInternal(task, task->distConfig.maxDistance);
        			s.setDist(distance);
        			// Also set the collision bit if distance is below 0.5mm
        			if(distance < 0.0005 and task->checkCol)
        			{
        				s.setCol(1);
        			}
        		}

                // Record result (state and collision) in grid
                task->statesGridWithInfo[task->dim_x*task->dim_y*z + task->dim_x*y + x] = s;
                //task->col_grid[task->dim_x*task->dim_y*z + task->dim_x*y + x] = col;

				//Explicitly delete temporary object
				s.~stateWithInfo();

	            //std::chrono::_V2::system_clock::time_point end = std::chrono::high_resolution_clock::now();
	            //std::cout << "Collision check, setting states, storing values took "
	            //          << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
	    		//		  << "us" << std::endl;
			}
		}
	}

	// Debug output
	/*
	for(int z=0; z<task->dim_z; z++)
	{
		for(int y=0; y<task->dim_y; y++)
		{
			for(int x=0; x<task->dim_x; x++)
			{
				// OUTPUT RESULT ON TERMINAL
				std::cout << "col_grid[" << x << "][" << y  << "][" << z << "]: " << task->statesGridWithInfo[task->dim_x*task->dim_y*z + task->dim_x*y + x].col << std::endl; // 1 if in collision 0 otherwise
			}
		}
	}
	*/
	// Restore state
	simSetObjectPosition(task->robotHandle, task->refFrameHandle, &pos_init[0]);
	simSetObjectOrientation(task->robotHandle, task->refFrameHandle, &orient_init[0]);

    std::chrono::_V2::system_clock::time_point end = std::chrono::high_resolution_clock::now();
    std::cout << "Collision check, visibility check, setting states, storing values took "
              << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
			  << "us" << std::endl;
}


// return the grid with info about collision or not
void getGridCol(SScriptCallBack *p, const char *cmd, getGridCol_in *in, getGridCol_out *out)
{
	TaskDef *task = getTask(in->taskHandle);

    for(size_t i = 0; i < task->dim_total; i++)
    {
    	//std::cout << "(int)task->col_grid[i] " << (int)task->col_grid[i] << std::endl;
    	//out->grid.push_back((int)task->col_grid[i]);
    	out->grid.push_back((int)task->statesGridWithInfo[i].col);
    }
}

void getGridVisTarget(SScriptCallBack *p, const char *cmd, getGridVisTarget_in *in, getGridVisTarget_out *out)
{
	TaskDef *task = getTask(in->taskHandle);

    for(size_t i = 0; i < task->dim_total; i++)
    {
    	//std::cout << "(int)task->col_grid[i] " << (int)task->col_grid[i] << std::endl;
    	//out->grid.push_back((int)task->col_grid[i]);
    	out->grid.push_back((int)task->statesGridWithInfo[i].vis_target);
    }
}

void getGridVisObj(SScriptCallBack *p, const char *cmd, getGridVisObj_in *in, getGridVisObj_out *out)
{
	TaskDef *task = getTask(in->taskHandle);

    for(size_t i = 0; i < task->dim_total; i++)
    {
    	//std::cout << "(int)task->col_grid[i] " << (int)task->col_grid[i] << std::endl;
    	//out->grid.push_back((int)task->col_grid[i]);
    	out->grid.push_back((int)task->statesGridWithInfo[i].vis_obj);
    }
}


// Add getGridDist
void getGridDist(SScriptCallBack *p, const char *cmd, getGridDist_in *in, getGridDist_out *out)
{
	TaskDef *task = getTask(in->taskHandle);

    for(size_t i = 0; i < task->dim_total; i++)
    {
    	//std::cout << "(int)task->col_grid[i] " << (int)task->col_grid[i] << std::endl;
    	//out->grid.push_back((int)task->col_grid[i]);
    	out->grid.push_back((float)task->statesGridWithInfo[i].dist);
    }
    out->maxDistance = task->distConfig.maxDistance;
}


// returns the vectors of values for each axis of the grid
void getGridAxesValues(SScriptCallBack *p, const char *cmd, getGridAxesValues_in *in, getGridAxesValues_out *out)
{
	TaskDef *task = getTask(in->taskHandle);

	for(int i=0; i < task->dim_x; i++)
	{
		//std::cout << "values_x" << (float)task->values_x[i] << std::endl;
		out->values_x.push_back((float)task->values_x[i]);
	}
	for(int i=0; i < task->dim_y; i++)
	{
		//std::cout << "values_y" << (float)task->values_y[i] << std::endl;
		out->values_y.push_back((float)task->values_y[i]);
	}
	for(int i=0; i < task->dim_z; i++)
	{
		//std::cout << "values_z" << (float)task->values_z[i] << std::endl;
		out->values_z.push_back((float)task->values_z[i]);
	}
}


// return a vector of states that have been checked (flattened vector of (x,y,z)), later add theta
void getStates(SScriptCallBack *p, const char *cmd, getStates_in *in, getStates_out *out)
{
	TaskDef *task = getTask(in->taskHandle);
    for(size_t i = 0; i < task->dim_total; i++)
    {
    	//std::cout << "(int)task->col_grid[i] " << (int)task->col_grid[i] << std::endl;
    	//out->grid.push_back((int)task->col_grid[i]);
    	out->states.push_back((float)task->statesGridWithInfo[i].x);
    	out->states.push_back((float)task->statesGridWithInfo[i].y);
    	out->states.push_back((float)task->statesGridWithInfo[i].z);
    }

}

// Later add rotation to state
bool stateInBounds(TaskDef *task, std::vector<simFloat> state)
{
	simFloat delta=0.0;
	for(int i=0; i<3; i++)
	{
		switch (i)
		{
			case 0:
				delta = task->dx;
				break;
			case 1:
				delta = task->dy;
				break;
			case 2:
				delta = task->dz;
				break;
			default:
				throw std::string("Unhandled case in stateInBounds.");
		}

		// Allowing the state to be half the grid delta size outsize of the grid
		if(state.at(i) < task->boundsLow.at(i)-delta or state.at(i) > task->boundsHigh.at(i)+delta)
		{
			return false;
		}
	}
	return true;
}

// What is the closest grid point for start and goal state?
// This finds the nearest neighbor grid point for the input state
void findClosestGridPoint(TaskDef *task, std::vector<simFloat> state_in, std::vector<simInt> *grid_coordinate_out)
{
	if (!stateInBounds(task,state_in))
	{
		for(int i=0; i<3; i++)
		{
			std::cout << "state_in.at(i) " << state_in.at(i)
					<< "Bounds:" << task->boundsLow.at(i)
					<< " " << task->boundsHigh.at(i) << std::endl;
		}
        throw std::string("State is out of bound.");
	}

	simInt x=0;
	simInt y=0;
	simInt z=0;
	//simInt t=0;

	simFloat dist_to_coordinate = INF;
	for(int i=0; i<task->dim_x; i++)
	{
		//std::cout << task->values_x.at(i) << " " << state_in.at(0) << std::endl;
		//std::cout << task->values_x.at(i)-state_in.at(0) << std::endl;
		//std::cout << "x, i=" << i << "dist" << fabs(task->values_x.at(i)-state_in.at(0)) << std::endl;
		if(fabs(task->values_x.at(i)-state_in.at(0)) < dist_to_coordinate)
		{
			grid_coordinate_out->at(0) = i;
			dist_to_coordinate = fabs(task->values_x.at(i)-state_in.at(0));
		}
	}

	dist_to_coordinate = INF;
	for(int i=0; i<task->dim_y; i++)
	{
		if(fabs(task->values_y.at(i)-state_in.at(1)) < dist_to_coordinate)
		{
			grid_coordinate_out->at(1) = i;
			dist_to_coordinate = fabs(task->values_y.at(i)-state_in.at(1));
		}
	}

	dist_to_coordinate = INF;
	for(int i=0; i<task->dim_z; i++)
	{
		if(fabs(task->values_z.at(i)-state_in.at(2)) < dist_to_coordinate)
		{
			grid_coordinate_out->at(2) = i;
			dist_to_coordinate = fabs(task->values_z.at(i)-state_in.at(2));
		}
	}
}

// there is already "setGoal" define by OMPL plugin
void setStateStart(SScriptCallBack *p, const char *cmd, setStateStart_in *in, setStateStart_out *out)
{
	TaskDef *task = getTask(in->taskHandle);
	if (!stateInBounds(task,in->stateStart))
	{
		for(int i=0; i<3; i++)
		{
			std::cout << "in->stateStart.at(i) " << in->stateStart.at(i)
					<< "Bounds:" << task->boundsLow.at(i)
					<< " " << task->boundsHigh.at(i) << std::endl;
		}
		throw std::string("State is out of bound.");
	}
	task->stateStart.clear();
	for(int i=0; i< in->stateStart.size(); i++ )
	{
		task->stateStart.push_back(in->stateStart.at(i));
	}
}


// there is already "setGoal" define by OMPL plugin
void setStateGoal(SScriptCallBack *p, const char *cmd, setStateGoal_in *in, setStateGoal_out *out)
{
	TaskDef *task = getTask(in->taskHandle);
	if (!stateInBounds(task,in->stateGoal))
	{
        throw std::string("State is out of bound.");
	}
	task->stateGoal.clear();
	for(int i=0; i< in->stateGoal.size(); i++ )
	{
		task->stateGoal.push_back(in->stateGoal.at(i));
	}
}


void setDistPenaltyParams(SScriptCallBack *p, const char *cmd, setDistPenaltyParams_in *in, setDistPenaltyParams_out *out)
{
	// Update parameters:
	// - distNoPenalty
	// - distPenaltyFactor (e.g. set to 0 to turn dist penalty off)
	TaskDef *task = getTask(in->taskHandle);


	if(in->distNoPenalty > task->distConfig.maxDistance)
	{
		std::cout << "Warning: distNoPenalty " << in->distNoPenalty << " is too large. "
				  << "The distance grid does not contain information about distances > "
				  << task->distConfig.maxDistance << " (maxDistance)."
				  << "distNoPenalty parameter has not been updated." << std::endl;
	}
	else
	{
		task->distConfig.distNoPenalty = in->distNoPenalty;		// factor for distance penalty (if dist below distNoPenalty)
	}

	task->distConfig.distPenaltyFactor = in->distPenaltyFactor;		// Threshold for distance penalty (if dist below distNoPenalty)
}


void getDistPenaltyParams(SScriptCallBack *p, const char *cmd, getDistPenaltyParams_in *in, getDistPenaltyParams_out *out)
{
	TaskDef *task = getTask(in->taskHandle);
	out->maxDistance = task->distConfig.maxDistance;
	out->distNoPenalty = task->distConfig.distNoPenalty;
	out->distPenaltyFactor = task->distConfig.distPenaltyFactor;
}

// explores all nodes on the grid and creates an array with indices pointing to the parent (where goal is the root with no parent)
void findAllPaths(SScriptCallBack *p, const char *cmd, findAllPaths_in *in, findAllPaths_out *out)
{
	//std::cout << "findAllPaths" << std::endl;
	std::chrono::_V2::system_clock::time_point start_time2 = std::chrono::high_resolution_clock::now();
	TaskDef *task = getTask(in->taskHandle);

	// get goal index in flattened grid for the goal state
	std::vector<simInt> grid_coordinate_goal = {-1,-1,-1};
	findClosestGridPoint(task,task->stateGoal,&grid_coordinate_goal);
	int goal = cood_1d(grid_coordinate_goal.at(0),grid_coordinate_goal.at(1),grid_coordinate_goal.at(2),task->dim_x,task->dim_y);
	//std::cout << "goal " << goal << std::endl;

	// input to astar
	task->cost_grid.clear();
	task->cost_grid.reserve(task->dim_total);
	task->cost_grid_dist.clear();
	task->cost_grid_dist.reserve(task->dim_total);


	// output to astar
	task->pathsAll.clear();
	task->pathsAll.reserve(task->dim_total);
	task->pathsAllCosts.clear();
	task->pathsAllCosts.reserve(task->dim_total);

	// copy collisions to a cost vector
	// initialize output vectors (for paths and costs)
	for(int i=0; i<task->dim_total; i++)
	{
		if(task->use_collision_cost_for_planner)
		{
			task->cost_grid.push_back(10*task->statesGridWithInfo[i].col); // the cost will be 10 for each grid point in collision, otherwise 0
		}
		else
		{
			task->cost_grid.push_back(0); // 0 cost for collision, instead use cost_grid_dist with sufficient high distPenaltyFactor so that planner avoids collisions
		}
		task->cost_grid_dist.push_back(task->statesGridWithInfo[i].dist);
		task->pathsAll.push_back(-1); // default is "no parent"
		task->pathsAllCosts.push_back(INF); // if never set actual cost for a state
	}

	bool success = false;
	// Time the execution of astar
	std::chrono::_V2::system_clock::time_point start_time = std::chrono::high_resolution_clock::now();

	//explore starting at goal to "everywhere"
	success = astar(&(task->cost_grid[0]),					// costs for each state (e.g. costs for collision, occlusion etc)
					task->dim_x, task->dim_y, task->dim_z,	// dimensions of the grid
			        task->dx, task->dy, task->dz,			// distance between grid points in each dimension
					goal,									// "start of astar"=goal (find all paths TO goal)
					-1,										// "goal of astar"=-1, find ALL paths
					&(task->cost_grid_dist[0]),             // distance to obstacles for each state
					task->distConfig.distPenaltyFactor,		// factor for distance penalty (if dist below distNoPenalty)
					task->distConfig.distNoPenalty,			// Threshold for distance penalty (if dist below distNoPenalty)
					&(task->pathsAll[0]),					// output for all paths
					&(task->pathsAllCosts[0])); 			// output for all costs

	//for(int i=0; i<task->dim_total; i++)
	//{
	//	std::cout << "pathsAll[" << i << "] " << task->pathsAll[i] << std::endl;
	//	std::cout << "pathsAllCosts[" << i << "] " << task->pathsAllCosts[i] << std::endl;
	//}
	std::chrono::_V2::system_clock::time_point end_time = std::chrono::high_resolution_clock::now();
    //std::cout << "astar took "
    //          << std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count()
	//		  << "us" << std::endl;
	//std::cout << "Success value: " << success << std::endl;

    std::cout << "findAllPaths took "
              << std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time2).count()
			  << "us" << std::endl;
}

// find the neighbor of state in grid point that has lowest cost (dist(state2grid)+costToGo(grid) )
// similar to findClosestGridPoint, but considering the costs2go
// can only use this for start state (not goal)
// Also this can be used AFTER calling findAllPaths (for queryPath or queryCostToGoal)
float findCheapestGridPoint(TaskDef *task, std::vector<simFloat> state_in, std::vector<simInt> *grid_coordinate_out)
{
	// Pseudo Code
	// find the corner grid point x,y,z with smallest x,y,z such that value(x)>state(x), (same for y, and z)
	// Iterate over all grid points of a,b,c where a in {x,x-1}, b in {y,y-1} and c in {z,z-1}
	// return the grid point with the lowest sum of costs= cost(gridpoint to goal)+ cost(state to gridpoint)
	if (!stateInBounds(task,state_in))
	{
        throw std::string("State is out of bound.");
	}

	int x=0;
	int y=0;
	int z=0;
	//simInt t=0;
	//std::cout << "state_in.at(0) " << state_in.at(0) << std::endl;
	//std::cout << "task->values_x.at(x) " << task->values_x.at(x)  << std::endl;
	while(x < task->values_x.size()-1 and state_in.at(0) >= task->values_x.at(x))
	{
		x++;
	}

	while(y < task->values_y.size()-1 and state_in.at(1) >= task->values_y.at(y))
	{
		y++;
	}
	//std::cout << "z " << z << std::endl;
	//std::cout << "state_in.at(2) " << state_in.at(2) << std::endl;
	//std::cout << "task->values_z.at(z) " << task->values_z.at(z) << std::endl;
	while(z < task->values_z.size()-1 and state_in.at(2) >= task->values_z.at(z))
	{
		//std::cout << "z " << z << std::endl;
		//std::cout << "state_in.at(2) " << state_in.at(2) << std::endl;
		//std::cout << "task->values_z.at(z) " << task->values_z.at(z) << std::endl;
		z++;
		//if(z == task->values_z.size())
		//{
		//	std::cout << "z == task->values_z.size(): breaking loop"  << std::endl;
		//	break;
		//}

	}


	//std::cout << "state_in.at(2) " << state_in.at(2) << std::endl;
	//std::cout << "task->values_z.at(z) " << task->values_z.at(z) << std::endl;

	//std::cout << "upper coordinates x,y,z " << x << " " << y << " " << z << std::endl;

	float x_v = 0;
	float y_v = 0;
	float z_v = 0;
	float total_dist2go = INF;
	float dist_to_grid = INF;
	float dist_to_grid_out = INF;
	float dist_to_goal = INF;
	int coord1D = 0;

	for(int x_coord=x-1; x_coord <= x; x_coord++)
	{
		for(int y_coord=y-1; y_coord <= y; y_coord++)
		{
			for(int z_coord=z-1; z_coord <= z; z_coord++)
			{
				// In case that the "upper coordinate is 0", then 'upper coordinate-1' is negative
				// This which would be the case if the state_in value is a little below the bounds
				if(x_coord < 0 or y_coord < 0 or z_coord < 0)
				{
					//std::cout << " Need to skip these cases, (negative indices not allowed)" << std::endl;
					continue;
				}
				x_v = task->values_x.at(x_coord);
				y_v = task->values_y.at(y_coord);
				z_v = task->values_z.at(z_coord);
				dist_to_grid = euclidean(state_in.at(0),state_in.at(1),state_in.at(2),x_v,y_v,z_v);
				coord1D = cood_1d(x_coord,y_coord,z_coord,task->dim_x,task->dim_y);
				dist_to_goal = task->pathsAllCosts.at(coord1D);
				//std::cout << "x,y,z " << x_coord << " " << y_coord << " " << z_coord << std::endl;
				//std::cout << "dist_to_grid " << dist_to_grid << std::endl;
				//std::cout << "dist_to_goal " << dist_to_goal << std::endl;
				//std::cout << "dist_to_grid+dist_to_goal " << dist_to_grid+dist_to_goal << std::endl;
				if((dist_to_grid+dist_to_goal) < total_dist2go)
				{

					grid_coordinate_out->at(0) = x_coord;
					grid_coordinate_out->at(1) = y_coord;
					grid_coordinate_out->at(2) = z_coord;
					total_dist2go = dist_to_grid+dist_to_goal;
					dist_to_grid_out = dist_to_grid;
					//std::cout << "total_dist2go " << total_dist2go << std::endl;
				}
			}
		}
	}
	/*
	for(int i=0; i<=2; i++)
	{
		std::cout << "grid_coordinate_out " <<  grid_coordinate_out->at(i) << std::endl;
	}
	*/

	if(dist_to_grid_out == INF)
	{
		throw std::string("dist_to_grid_out == INF");
	}
	return dist_to_grid_out;


}

// can call this after findAllPaths (it expects pathsAll and pathsAllCosts with values)
void queryPath(SScriptCallBack *p, const char *cmd, queryPath_in *in, queryPath_out *out)
{
	//std::cout << "queryPath" << std::endl;
	//std::chrono::_V2::system_clock::time_point start_time = std::chrono::high_resolution_clock::now();

	TaskDef *task = getTask(in->taskHandle);

	std::vector<simInt> grid_coordinate_start_old = {-1,-1,-1};


	//float cost_start_to_grid = findCheapestGridPoint(task,task->stateStart,&grid_coordinate_start_old);
	//std::cout << "Cheapest grid point" << std::endl;
	//std::cout << "grid_coordinate_start.at(0),grid_coordinate_start.at(1),grid_coordinate_start.at(2)"
	//					<< grid_coordinate_start_old.at(0) << " "
	//					<< grid_coordinate_start_old.at(1) << " "
	//					<< grid_coordinate_start_old.at(2) << std::endl;

	std::vector<simInt> grid_coordinate_start = {-1,-1,-1};
	float cost_start_to_grid = 0.0;
	findClosestGridPoint(task,task->stateStart,&grid_coordinate_start);
	//std::cout << "Closest grid point" << std::endl;
	//std::cout << "grid_coordinate_start.at(0),grid_coordinate_start.at(1),grid_coordinate_start.at(2)"
	//					<< grid_coordinate_start.at(0) << " "
	//					<< grid_coordinate_start.at(1) << " "
	//					<< grid_coordinate_start.at(2) << std::endl;

	int start = cood_1d(grid_coordinate_start.at(0),grid_coordinate_start.at(1),grid_coordinate_start.at(2),task->dim_x,task->dim_y);
	//std::cout << "start " << start << std::endl;

	std::vector<int> path_indices;
	path_indices.push_back(start);
	int parent = task->pathsAll.at(start);
	while(parent != -1) // goal has no parent
	{
		path_indices.push_back(parent);
		parent = task->pathsAll.at(parent);
	}

	// Debugging output to terminal
	//std::cout << "path (list of indices) in order (start to goal): " << std::endl;
	//for (int i=0; i<path_indices.size(); i++)
	//{
	//    std::cout << "i :" << i
	//    		  << " path index: " << path_indices.at(i)
	//    		  << " cost at path index: " << task->pathsAllCosts.at(path_indices.at(i))
	//			  << std::endl;
	//}
	//std::cout << std::endl;

    // store and output path to V-REP
	task->pathStates.clear();
	int x_coord = 0;
	int y_coord = 0;
	int z_coord = 0;
	simFloat x_v = 0;
	simFloat y_v = 0;
	simFloat z_v = 0;

	float cost_grid_to_goal = 0.0;

	for (int i=0; i<path_indices.size(); i++)
	{
		// update x_coord,y_coord,z_coord
		coordinate_1D_to_3D(path_indices.at(i),task->dim_x,task->dim_y, x_coord, y_coord, z_coord);
		//std::cout << "path_indices.at(i) " << path_indices.at(i) << "x,y,z cood " << x_coord << ' ' << y_coord << ' ' << z_coord << std::endl;
		x_v = task->values_x.at(x_coord);
		y_v = task->values_y.at(y_coord);
		z_v = task->values_z.at(z_coord);
		/*
		if(i == 0)
		{
			// add start state (which is not perfectly on grid)
			std::cout << task->stateStart.at(0) << ' ' << x_v << ' ' << task->stateStart.at(1) << ' ' << y_v << ' ' << task->stateStart.at(2) << ' ' << z_v << std::endl;
			cost_start_to_grid = euclidean(task->stateStart.at(0),task->stateStart.at(1),task->stateStart.at(2),x_v,y_v,z_v);
			std::cout << "cost_start_to_grid " << cost_start_to_grid << std::endl;
		}
		*/
		if(i == path_indices.size()-1)
		{
			// add goal state (which is not perfectly on grid if grid was not aligned with goal, see V-REP script)
			//std::cout << "stateGoal (value at gird point and goal value)" <<
			//		task->stateGoal.at(0) << ' ' << x_v << ' ' << task->stateGoal.at(1) << ' ' << y_v << ' ' << task->stateGoal.at(2) << ' ' << z_v << std::endl;

			cost_grid_to_goal = euclidean(task->stateGoal.at(0),task->stateGoal.at(1),task->stateGoal.at(2),x_v,y_v,z_v);
			if(cost_grid_to_goal > 0.001)
			{
				std::cout << "cost_grid_to_goal " << cost_grid_to_goal << std::endl;
				std::cout << "This is currently not recommended "<< std::endl;
				std::cout << "Make sure that goal is located (close) to gridpoint (e.g. my translating the grid)."<< std::endl;
			}
			//std::cout << "cost_grid_to_goal " << cost_grid_to_goal << std::endl;
		}


		//std::cout << "x,y,z values " << x_v << ' ' << y_v << ' ' << z_v << std::endl;

		task->pathStates.push_back(x_v);
		task->pathStates.push_back(y_v);
		task->pathStates.push_back(z_v);
		// also return to V-REP
		out->path.push_back(x_v);
		out->path.push_back(y_v);
		out->path.push_back(z_v);
	}


	// add goal state (which is not perfectly on grid)
	task->pathStates.push_back(task->stateGoal.at(0));
	task->pathStates.push_back(task->stateGoal.at(1));
	task->pathStates.push_back(task->stateGoal.at(2));
	// also return to V-REP
	out->path.push_back(task->stateGoal.at(0));
	out->path.push_back(task->stateGoal.at(1));
	out->path.push_back(task->stateGoal.at(2));


    // output path cost

    out->path_cost =  cost_start_to_grid + task->pathsAllCosts.at(start) + cost_grid_to_goal;
	//std::cout << "total path cost (query path): " << out->path_cost << std::endl;

    // output duration of function call (to terminal)
	//std::chrono::_V2::system_clock::time_point end_time = std::chrono::high_resolution_clock::now();
	//std::cout << "queryPath took "
	//		  << std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count()
	//		  << "us" << std::endl;
}

// can call this after findAllPaths (it expects pathsAll and pathsAllCosts with values)
// similar to queryPath, but only returns the cost (not the path) so it should be much faster
void queryCostToGoal(SScriptCallBack *p, const char *cmd, queryCostToGoal_in *in, queryCostToGoal_out *out)
{
	std::cout << "queryCostToGoal" << std::endl;
	std::chrono::_V2::system_clock::time_point start_time = std::chrono::high_resolution_clock::now();

	TaskDef *task = getTask(in->taskHandle);

	std::vector<simInt> grid_coordinate_start = {-1,-1,-1};
	float cost_start_to_grid = findCheapestGridPoint(task,task->stateStart,&grid_coordinate_start);


	int start = cood_1d(grid_coordinate_start.at(0),grid_coordinate_start.at(1),grid_coordinate_start.at(2),task->dim_x,task->dim_y);
	std::cout << "start " << start << std::endl;

	std::vector<simInt> grid_coordinate_goal = {-1,-1,-1};
	findClosestGridPoint(task,task->stateGoal,&grid_coordinate_goal);
	int goal = cood_1d(grid_coordinate_goal.at(0),grid_coordinate_goal.at(1),grid_coordinate_goal.at(2),task->dim_x,task->dim_y);
	std::cout << "goal " << goal << std::endl;


	float cost_grid_to_goal = 0.0;
	int x_coord = 0;
	int y_coord = 0;
	int z_coord = 0;
	simFloat x_v = 0.0;
	simFloat y_v = 0.0;
	simFloat z_v = 0.0;

	// cost_start_to_grid (for start which is not perfectly on grid)
	/*
	coordinate_1D_to_3D(start,task->dim_x,task->dim_y, x_coord, y_coord, z_coord);
	x_v = task->values_x.at(x_coord);
	y_v = task->values_y.at(y_coord);
	z_v = task->values_z.at(z_coord);
	std::cout << task->stateStart.at(0) << ' ' << x_v << ' ' << task->stateStart.at(1) << ' ' << y_v << ' ' << task->stateStart.at(2) << ' ' << z_v << std::endl;
	cost_start_to_grid = euclidean(task->stateStart.at(0),task->stateStart.at(1),task->stateStart.at(2),x_v,y_v,z_v);
	std::cout << "cost_start_to_grid " << cost_start_to_grid << std::endl;
	std::cout << "cost_start_to_grid_better (should be the same as above)" << cost_start_to_grid_better << std::endl;
	*/

	// cost_goal_to_grid (for goal which is not perfectly on grid)
	coordinate_1D_to_3D(goal,task->dim_x,task->dim_y, x_coord, y_coord, z_coord);
	x_v = task->values_x.at(x_coord);
	y_v = task->values_y.at(y_coord);
	z_v = task->values_z.at(z_coord);
	//std::cout << task->stateGoal.at(0) << ' ' << x_v << ' ' << task->stateGoal.at(1) << ' ' << y_v << ' ' << task->stateGoal.at(2) << ' ' << z_v << std::endl;
	cost_grid_to_goal = euclidean(task->stateGoal.at(0),task->stateGoal.at(1),task->stateGoal.at(2),x_v,y_v,z_v);
	std::cout << "cost_grid_to_goal " << cost_grid_to_goal << std::endl;

    // output path cost
    out->path_cost =  cost_start_to_grid + task->pathsAllCosts.at(start) + cost_grid_to_goal;
	std::cout << "total path cost (queryCostToGoal): " << out->path_cost << std::endl;

    // output duration of function call (to terminal)
	std::chrono::_V2::system_clock::time_point end_time = std::chrono::high_resolution_clock::now();
	std::cout << "queryCostToGoal took "
			  << std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count()
			  << "us" << std::endl;
}

// do not use, it might overestimate the distance to goal (because Can't connect to cheapest grid point with findCheapestGridPoint at start since the costs haven't calculated for all grid points)
// This doesn't require to findAllPaths first. It only explores the nodes needed (using euclidean heuristic) until goal found
// This seems to have some extra code for testing/debugging
void findPath(SScriptCallBack *p, const char *cmd, findPath_in *in, findPath_out *out)
{
	TaskDef *task = getTask(in->taskHandle);
	//std::vector<simFloat> start_state = {0.15,0.6,0.31};
	std::vector<simInt> grid_coordinate_start = {-1,-1,-1};
	findClosestGridPoint(task,task->stateStart,&grid_coordinate_start);
	std::vector<simInt> grid_coordinate_goal = {-1,-1,-1};
	findClosestGridPoint(task,task->stateGoal,&grid_coordinate_goal);



	//for(int i=0; i<grid_coordinate.size();i++)
	//	std::cout << grid_coordinate.at(i) << std::endl;

	// try a dummy "maze" first
	bool success = false;
	float weights[] = {0,0,0,
			           0,0,0,
					   0,10,0}; // the flattened grid of weights (costs of being in each state)
	float dists[] =  {2.2, 2.0, 2.2,
			          1.4, 1,   1.4,
					  1,   0,   1};
	// the weight should be high if in collision, occlusion, or near obstacles
	// dimensions of the grid
	int y = 3;
	int x = 3;
	int z = 1;
	// start index in the flattened grid
	int start = 1;
	// goal index in the flattened grid
	int goal = 6;
	// distance between grid points in all axis directions ("resolution" of the grid)
	float dx = 1.0;
	float dy = 1.0;
	float dz = 1.0;

	float distPenaltyFactor = 1.0;
	float distNoPenalty = 2.0;
	// this is passed to astar and will contain the paths (solution). Must have size of the flattened grid.
	int paths[15]; // this specifies the parent index of point in the flattened grid (after path was found)
	float costs_out[15];
	std::cout << "Start, Goal: " << start << ' ' << goal << std::endl;
	for(int i=0; i<x*y*z; i++)
	{
		paths[i] = -1; // default is "no parent"
		costs_out[i] = -1; // -1 means that it was never calculated for this state
	}
	success = astar(weights, x, y, z, dx, dy, dz, start, goal,
					dists, distPenaltyFactor, distNoPenalty, paths,costs_out);
	std::cout << "Success (path found): " << success << std::endl;
	for(int i=0; i<x*y*z; i++)
	{
		std::cout << "path[" << i << "]" << paths[i] << std::endl;
	}

	for(int i=0; i<x*y*z; i++)
	{
		std::cout << "costs_out[" << i << "]" << costs_out[i] << std::endl;
	}

	// Calculate the actual path (order of indices in flattened grid)
	std::vector<int> path_indices;
	path_indices.push_back(goal);
	int parent = paths[goal];
	while(parent != -1) // start has no parent
	{
		path_indices.push_back(parent);
		parent = paths[parent];
	}


	std::cout << "path (list of indices) in order : " << std::endl;
	for (int i=path_indices.size()-1; i>=0; i--)
	{
	    //std::cout << path_indices.at(i) << " ";
	    std::cout << "i :" << i << "path index " << path_indices.at(i) << "cost at path index " << costs_out[path_indices.at(i)] << std::endl;
	}
	std::cout << std::endl;


	// Try to find all paths to goal (for all start indices) #############################
	// with reversed start and goal
	start = 6;
	// goal index in the flattened grid
	goal = 1;
	std::cout << "Start, Goal: " << start << ' ' << -1 << std::endl;
	for(int i=0; i<x*y*z; i++)
	{
		paths[i] = -1; // default is "no parent"
		costs_out[i] = -1; // -1 means that it was never calculated for this state
	}
	success = astar(weights, x, y, z, dx, dy, dz, start, -1,
			        dists, distPenaltyFactor, distNoPenalty, paths,costs_out); // star_all_path implemented in astar with goal <0
	//success = astar_all_paths(weights, x, y, z, dx, dy, dz, start, goal, paths,costs_out);
	std::cout << "Success (path found): " << success << std::endl;
	for(int i=0; i<x*y*z; i++)
	{
		std::cout << "path[" << i << "]" << paths[i] << std::endl;
	}

	for(int i=0; i<x*y*z; i++)
	{
		std::cout << "costs_out[" << i << "]" << costs_out[i] << std::endl;
	}
	// Calculate the actual path (order of indices in flattened grid)
	path_indices.clear();
	path_indices.push_back(goal);
	parent = paths[goal];
	while(parent != -1) // goal has no parent
	{
		path_indices.push_back(parent);
		parent = paths[parent];
	}


	std::cout << "path (list of indices) in order : " << std::endl;
	for (int i=path_indices.size()-1; i>=0; i--)
	{
	    //std::cout << path_indices.at(i) << " ";
	    std::cout << "i :" << i << "path index " << path_indices.at(i) << "cost at path index " << costs_out[path_indices.at(i)] << std::endl;
	}
	std::cout << std::endl;

	// with the actual grid from V-REP ########################
	task->cost_grid.clear();
	task->cost_grid.reserve(task->dim_total);
	// copy collisions to a cost vector
	for(int i=0; i<task->dim_total; i++)
	{
       task->cost_grid.push_back(10*task->statesGridWithInfo[i].col); // the cost will be 10 for each grid point in collision
	}
	std::vector<int> paths_big;
	paths_big.reserve(task->dim_total);
	std::vector<float> costs_out_big;
	costs_out_big.reserve(task->dim_total);
	std::vector<float> dists_big;
	dists_big.reserve(task->dim_total);
	for(int i=0; i<task->dim_total; i++)
	{
		paths_big.push_back(-1); // default is "no parent"
		costs_out_big.push_back(INF); // if never set actual cost for a state
		dists_big.push_back(0.1); // some dummy distance of 0.1 meters for all grid points
	}
	start = cood_1d(grid_coordinate_start.at(0),grid_coordinate_start.at(1),grid_coordinate_start.at(2),task->dim_x,task->dim_y);
	goal = cood_1d(grid_coordinate_goal.at(0),grid_coordinate_goal.at(1),grid_coordinate_goal.at(2),task->dim_x,task->dim_y);
	std::cout << "start " << start << " goal " << goal << std::endl;

	std::chrono::_V2::system_clock::time_point start_time = std::chrono::high_resolution_clock::now();

	success = astar(&(task->cost_grid[0]), task->dim_x, task->dim_y, task->dim_z,
			                               task->dx, task->dy, task->dz,
										   start, goal,
										   &dists_big[0], distPenaltyFactor, distNoPenalty,
										   &paths_big[0], &costs_out_big[0]); //"start"=goal (find all paths TO goal), goal=-1, find ALL paths
	std::chrono::_V2::system_clock::time_point end_time = std::chrono::high_resolution_clock::now();
    std::cout << "astar took "
              << std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count()
			  << "us" << std::endl;
	std::cout << "Success (path found for real matrix): " << success << std::endl;

	// output the path, with costs for each state
	path_indices.clear();
	path_indices.push_back(goal);
	parent = paths_big.at(goal);
	while(parent != -1) // start has no parent
	{
		path_indices.push_back(parent);
		parent = paths_big.at(parent);
	}
	std::cout << "path (list of indices) in order : " << std::endl;
	for (int i=path_indices.size()-1; i>=0; i--)
	{
	    std::cout << "i :" << i << " path index: " << path_indices.at(i) << " cost at path index: " << costs_out_big.at(path_indices.at(i)) << std::endl;
	}
	std::cout << std::endl;

	task->pathStates.clear();
	int x_coord = 0;
	int y_coord = 0;
	int z_coord = 0;
	simFloat x_v = 0;
	simFloat y_v = 0;
	simFloat z_v = 0;

	float cost_start_to_grid = 0.0;
	float cost_grid_to_goal = 0.0;
	for (int i=path_indices.size()-1; i>=0; i--)
	{
		// update x_coord,y_coord,z_coord
		coordinate_1D_to_3D(path_indices.at(i),task->dim_x,task->dim_y, x_coord, y_coord, z_coord);
		//std::cout << "path_indices.at(i) " << path_indices.at(i) << "x,y,z cood " << x_coord << ' ' << y_coord << ' ' << z_coord << std::endl;
		x_v = task->values_x.at(x_coord);
		y_v = task->values_y.at(y_coord);
		z_v = task->values_z.at(z_coord);
		if(i == path_indices.size()-1)
		{
			// add start state (which is not perfectly on grid)
			std::cout << task->stateStart.at(0) << ' ' << x_v << ' ' << task->stateStart.at(1) << ' ' << y_v << ' ' << task->stateStart.at(2) << ' ' << z_v << std::endl;
			cost_start_to_grid = euclidean(task->stateStart.at(0),task->stateStart.at(1),task->stateStart.at(2),x_v,y_v,z_v);
			std::cout << "cost_start_to_grid " << cost_start_to_grid << std::endl;
		}
		if(i == 0)
		{
			// add goal state (which is not perfectly on grid)
			std::cout << task->stateGoal.at(0) << ' ' << x_v << ' ' << task->stateGoal.at(1) << ' ' << y_v << ' ' << task->stateGoal.at(2) << ' ' << z_v << std::endl;
			cost_grid_to_goal = euclidean(task->stateGoal.at(0),task->stateGoal.at(1),task->stateGoal.at(2),x_v,y_v,z_v);
			std::cout << "cost_grid_to_goal " << cost_grid_to_goal << std::endl;
		}


		//std::cout << "x,y,z values " << x_v << ' ' << y_v << ' ' << z_v << std::endl;

		task->pathStates.push_back(x_v);
		task->pathStates.push_back(y_v);
		task->pathStates.push_back(z_v);
		// also return to V-REP
		out->path.push_back(x_v);
		out->path.push_back(y_v);
		out->path.push_back(z_v);
	}


	// add goal state (which is not perfectly on grid)
	task->pathStates.push_back(task->stateGoal.at(0));
	task->pathStates.push_back(task->stateGoal.at(1));
	task->pathStates.push_back(task->stateGoal.at(2));
	// also return to V-REP
	out->path.push_back(task->stateGoal.at(0));
	out->path.push_back(task->stateGoal.at(1));
	out->path.push_back(task->stateGoal.at(2));


	out->path_cost =  cost_start_to_grid + costs_out_big.at(goal) + cost_grid_to_goal;
	std::cout << "total path cost: " << out->path_cost << std::endl;
}



class Plugin : public vrep::Plugin
{
public:
    void onStart()
    {
        if(!registerScriptStuff())
            throw std::runtime_error("failed to register script stuff");

        simSetModuleInfo(PLUGIN_NAME, 0, "PlannerGrid (Plan trajectories on grid avoiding collisions)  Plugin", 0);
        simSetModuleInfo(PLUGIN_NAME, 1, BUILD_DATE, 0);
    	std::cout << "BUILD_DATE " << BUILD_DATE << std::endl;
    	std::cout << "Last changes: adding checkVisibilityNoTask (fct) and use_collision_cost_for_planner (input parameter)" << std::endl;

    }

    void onSimulationEnded()
    {
        destroyTransientObjects();
    }
};

VREP_PLUGIN(PLUGIN_NAME, PLUGIN_VERSION, Plugin)
