# Plugin PlannerGrid for V-REP

This is a plugin that plans collision free trajectories of an object to a goal position/orientation.
It works as follows. The workspace is sampled on a grid (3D euclidean positions for now).
For each grid point the state is checked for collision (optional for occlusions too).
Then A* or uniform cost search is used to find the a collision free trajectories.
The collision check on the grid takes significant time (e.g. for 35k grid points about 200 seconds).
But then a query of trajectories for different initial positions to a fixed target position is very fast (in the order of micro seconds).



### Compiling

1. Install required packages for [v_repStubsGen](https://github.com/CoppeliaRobotics/v_repStubsGen): see v_repStubsGen's [README](external/v_repStubsGen/README.md)
2. Checkout and compile
```
$ git clone https://uliv@bitbucket.org/uliv/v_repextplannergrid.git
$ cmake .
$ cmake --build .
```
3. Copy compiled plugin libv_repExtPlannerGrid.so to V-REP root folder.
